data/intset
===========

An implementation of integer sets using [Discrete Interval Encoding
Trees](https://web.engr.oregonstate.edu/~erwig/diet/). This data structure
offers very efficient storage of consecutive ranges of integers - `1-10000000`
takes just as much storage space as `1-10` does.
